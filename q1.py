def baskara(a,b,c)
   delta = (b*b-4*a*c)
   if (delta<0):
      resposta = 1
   else:
      resposta = 0
      raiz1 = (-b+(delta**(1/2)))/(2*a)
      raiz2 = (-b-(delta**(1/2)))/(2*a)
      print('As raizes desta equação são: ',raiz1,' e ',raiz2,sep='')
   return resposta

def main():
   print('Insira os valores dos coeficientes a, b e c da equação:')
   a = int(input())
   b = int(input())
   c = int(input())
   baskara(a,b,c)

main()